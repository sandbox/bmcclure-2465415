<?php
/**
 * Created by PhpStorm.
 * User: BMcClure
 * Date: 4/4/2015
 * Time: 12:19 AM
 *
 * @file slick_magnific_gallery/includes/slick_magnific_gallery.formatters.inc
 *
 * Formatters for Slick Magnific Gallery.
 */

/**
 * @return array
 *
 * Implements hook_field_formatter_info().
 */
function slick_magnific_gallery_field_formatter_info() {
  $gallery_options = _magnific_popup_gallery_options();

  return array(
    'slick_magnific_gallery_file_field_formatter' => array(
      'label' => t('Slick Magnific Gallery'),
      'field types' => array('file', 'image', 'media'),
      'settings' => array(
        'gallery_style' => key($gallery_options),
        'small_thumbnail_image_style' => '',
        'large_thumbnail_image_style' => '',
        'full_image_style' => '',
        //'slick_optionset' => 'default',
        //'slick_optionset_thumbnail' => 'default',
      ),
    ),
  );
}

/**
 * @param $field
 * @param $instance
 * @param $view_mode
 * @param $form
 * @param $form_state
 * @return array
 *
 * Implements hook_field_formatter_settings_form().
 */
function slick_magnific_gallery_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $settings = $instance['display'][$view_mode]['settings'];

  $gallery_available = $field['cardinality'] !== 1;
  /*$optionsets = slick_optionset_options();*/
  $image_styles = image_style_options(FALSE, PASS_THROUGH);

  $element['gallery_available'] = array(
    '#type' => 'markup',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => _slick_magnific_gallery_available_markup($gallery_available),
  );

  $element['gallery_style'] = array(
    '#type' => 'select',
    '#title' => t('Gallery Type'),
    '#options' => _magnific_popup_gallery_options(),
    '#disabled' => !$gallery_available,
    '#default_value' => $settings['gallery_style'],
    '#description' => t('Choose how this Magnific gallery displays its triggering element(s).'),
  );

  /*$elements['slick_optionset'] = array(
    '#title' => t('Option set main'),
    '#type' => 'select',
    '#options' => $optionsets,
    '#default_value' => $settings['slick_optionset'],
    '#description' => t('Manage optionsets at <a href="@link" target="_blank">Slick carousel admin page</a>.', array('@link' => url('admin/config/media/slick'))),
  );*/

  /*$elements['slick_optionset_thumbnail'] = array(
    '#title' => t('Option set main'),
    '#type' => 'select',
    '#options' => $optionsets,
    '#default_value' => $settings['slick_optionset_thumbnail'],
    '#description' => t('Manage optionsets at <a href="@link" target="_blank">Slick carousel admin page</a>.', array('@link' => url('admin/config/media/slick'))),
  );*/

  $element['small_thumbnail_image_style'] = array(
    '#title' => t('Small thumbnail style'),
    '#type' => 'select',
    '#default_value' => $settings['small_thumbnail_image_style'],
    '#disabled' => !$gallery_available,
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#description' => t('Select the image style to be used for the small thumbnails.'),
  );

  $element['large_thumbnail_image_style'] = array(
    '#title' => t('Large thumbnail style'),
    '#type' => 'select',
    '#default_value' => $settings['large_thumbnail_image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#description' => t('Select the image style to use for the large thumbnails.'),
  );

  $element['full_image_style'] = array(
    '#title' => t('Full image style'),
    '#type' => 'select',
    '#default_value' => $settings['full_image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#description' => t('Select the image style to be used for the full image popups'),
  );

  return $element;
}

/**
 * @param $field
 * @param $instance
 * @param $view_mode
 * @return null|string
 *
 * Implements hook_field_formatter_settings_summary().
 */
function slick_magnific_gallery_field_formatter_settings_summary($field, $instance, $view_mode) {
  $settings = $instance['display'][$view_mode]['settings'];

  $gallery_options = _magnific_popup_gallery_options();

  $summary = array();

  $summary[] = t('Gallery type: @gallery_type', array('@gallery_type' => $gallery_options[$settings['gallery_style']]));
  $summary[] = t('Small thumbnail style: @style', array('@style' => $settings['small_thumbnail_image_style']));
  $summary[] = t('Large thumbnail style: @style', array('@style' => $settings['large_thumbnail_image_style']));
  $summary[] = t('Full image style: @style', array('@style' => $settings['full_image_style']));
  /*$summary[] = t('Slick optionset: @optionset', array('@optionset' => $settings['slick_optionset']));
  $summary[] = t('Slick thumbnail optionset: @optionset', array('@optionset' => $settings['slick_optionset_thumbnail']));*/

  return implode('<br />', $summary);
}

/**
 * @param $entity_type
 * @param $entities
 * @param $field
 * @param $instances
 * @param $langcode
 * @param $items
 * @param $displays
 *
 * Implements hook_field_formatter_prepare_view().
 *
 * Items are some kind of entity reference, be it via image or file type.
 * Load the corresponding items into a common format for our formatter_view().
 *
 * Iterate through $items looking for valid gallery items. Refactor them into
 * a consistent format for magnific_popup_field_formatter_view().
 */
function slick_magnific_gallery_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays) {
  foreach ($entities as $entity_id => $entity) {
    foreach ($items[$entity_id] as $delta => &$item) {
      $settings = $displays[$entity_id]['settings'];

      $item = _slick_magnific_gallery_prepare_item($item, $settings, $field['type']);

      if (empty($item)) {
        unset($items[$entity_id][$delta]);
      }
    }
  }
}

/**
 * @param $entity_type
 * @param $entity
 * @param $field
 * @param $instance
 * @param $langcode
 * @param $items
 * @param $display
 * @return array|bool
 *
 * Implements hook_field_formatter_view().
 */
function slick_magnific_gallery_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  if (!_magnific_popup_check_status()) {
    return FALSE;
  }

  slick_magnific_gallery_load_libraries();

  return _slick_magnific_gallery_load_items($field, $items, $display);
}

/**
 *
 */
function slick_magnific_gallery_load_libraries() {
  drupal_add_library('magnific_popup', 'magnific_popup_formatter', FALSE);

  _magnific_popup_add_api_js();

  slick_add();

  drupal_add_js(drupal_get_path('module', 'slick_magnific_gallery') .'/slick_magnific_gallery.js');

  drupal_add_css(drupal_get_path('module', 'slick_magnific_gallery') .'/slick_magnific_gallery.css');
}

/**
 * @param bool $gallery_available
 * @return null|string
 */
function _slick_magnific_gallery_available_markup($gallery_available) {
  $template = 'This field <strong>%s multiple items</strong> and %s display a gallery of items.';

  return $gallery_available ?
    t(sprintf($template, 'supports', 'can')) :
    t(sprintf($template, 'does not support', 'cannot'));
}

/**
 * @param $field
 * @param $items
 * @param $display
 * @return array
 */
function _slick_magnific_gallery_load_items($field, $items, $display) {
  $element = array();

  if (count($items)) {
    $element[0] = _slick_magnific_gallery_gallery($display, $field, $items);
  }

  return $element;
}

/**
 * @param $item
 * @param $settings
 * @return bool
 */
function _slick_magnific_gallery_prepare_item($item, $settings, $type) {
  $loaded_items = &drupal_static(__FUNCTION__, array());

  if (empty($item['uri']) && empty($item['file']) && empty($item['file']->uri)) {
    return FALSE;
  }

  // Check if we've already parsed this fid, and build it if not.
  if (!isset($loaded_items[$item['fid']])) {
    $loaded_items[$item['fid']] = _slick_magnific_gallery_load_item($item, $settings, $type);
  }

  // Replace $item with the parsed version of info for this fid.
  return $loaded_items[$item['fid']];
}

/**
 * @return array
 */
function _slick_magnific_gallery_thumbnail_schemes() {
  $thumbnail_schemes = &drupal_static(__FUNCTION__);

  if (!isset($thumbnail_schemes)) {
    $thumbnail_schemes = module_invoke_all('magnific_popup_thumbnail_schemes');
  }

  return $thumbnail_schemes;
}

function _slick_magnific_gallery_get_property($item, $property) {
  if (isset($item[$property])) {
    return $item[$property];
  }

  if (isset($item['file']) && isset($item['file']->$property)) {
    return $item['file']->$property;
  }

  return '';
}

/**
 * @param $item
 * @param $settings
 * @return array|bool
 */
function _slick_magnific_gallery_load_item(&$item, $settings, $type) {
  $uri = _slick_magnific_gallery_get_property($item, 'uri');

  $title = _slick_magnific_gallery_get_property($item, 'title');
  $filename = _slick_magnific_gallery_get_property($item, 'filename');
  $alt = _slick_magnific_gallery_get_property($item, 'alt');

  $scheme = file_uri_scheme($uri);

  /** @var $wrapper DrupalStreamWrapperInterface */
  $wrapper = _slick_magnific_gallery_wrapper($item, $scheme, $uri);

  if (!$wrapper) {
    return FALSE;
  }

  $small_thumbnail_path = _slick_magnific_gallery_image_path($wrapper, $scheme, $settings['small_thumbnail_image_style']);
  $large_thumbnail_path = _slick_magnific_gallery_image_path($wrapper, $scheme, $settings['large_thumbnail_image_style']);
  $full_image_path = _slick_magnific_gallery_image_path($wrapper, $scheme, $settings['full_image_style']);

  $full_uri = ($type == 'image') ? $full_image_path : $wrapper->getExternalUrl();

  return array(
    'item' => $item,
    'title' => $title ?: $filename,
    'alt' => $alt ?: $title ?: $filename,
    'small_thumbnail_path' => $small_thumbnail_path,
    'large_thumbnail_path' => $large_thumbnail_path,
    'target_uri' => $full_uri,
  );
}

/**
 * @param $item
 * @param $scheme
 * @return bool
 */
function _slick_magnific_gallery_wrapper(&$item, $scheme, $uri) {
  $wrappers = &drupal_static(__FUNCTION__, array());

  /** @var DrupalStreamWrapperInterface $wrapper */
  if (empty($wrappers[$scheme])) {
    $wrappers[$scheme] = file_stream_wrapper_get_instance_by_uri($uri);
    $wrapper = $wrappers[$scheme];
  } else {
    $wrapper = $wrappers[$scheme];
    $wrapper->setUri($uri);
  }

  return _slick_magnific_gallery_validate_wrapper($wrapper, $item, $uri);
}

/**
 * @param $wrapper
 * @param $item
 * @return bool
 *
 * Check that the resource is accessible.
 */
function _slick_magnific_gallery_validate_wrapper($wrapper, $item, $uri) {
  /** @var DrupalStreamWrapperInterface $wrapper */
  if ($wrapper && $wrapper->url_stat($uri, STREAM_URL_STAT_QUIET) !== FALSE) {
    return $wrapper;
  } else {
    return FALSE;
  }
}

/**
 * @param $wrapper
 * @param $scheme
 * @param $style
 * @return string Determine the file's info and store it.
 *
 * Determine the file's info and store it.
 */
function _slick_magnific_gallery_image_path($wrapper, $scheme, $style) {
  $image_path = FALSE;

  /** @var DrupalStreamWrapperInterface $wrapper */
  if (method_exists($wrapper, 'getLocalThumbnailPath')) {
    $image_path = $wrapper->getLocalThumbnailPath();
  } elseif ($uri = $wrapper->getUri()) {
    $image_path = $uri;
  }

  $style = _slick_magnific_gallery_image_style($scheme, $style);

  if ($style) {
    $url = image_style_url($style, $image_path);
  } else {
    $url = file_create_url($image_path);
  }

  return $url;
}

/**
 * @param $scheme
 * @param $style
 * @return string Adjust $thumbnail_path based on the $scheme.
 *
 * Adjust $thumbnail_path based on the $scheme.
 */
function _slick_magnific_gallery_image_style($scheme, $style) {
  $image_schemes = _slick_magnific_gallery_thumbnail_schemes();

  $image_style = $style;

  /*if (!empty($image_schemes[$scheme])) {
    $image_style = $image_schemes[$scheme];
  }*/

  return $image_style;
}

/**
 * @param $settings
 * @param $field
 * @return string
 *
 * Figure out the base-class for the gallery/nogallery, which is generally
 * something like 'mfp-nogallery-image' or 'mfp-gallery-iframe'.
 */
function _slick_magnific_gallery_base_class($settings, $field) {
  $class = $settings['gallery_style'] == 'no_gallery'
    ? 'mfp-nogallery'
    : 'mfp-gallery';

  $class .= ($field['type'] == 'image')
    ? '-image'
    : '-iframe';

  return $class;
}

/**
 * @param $display
 * @param $field
 * @param $items
 * @return array
 */
function _slick_magnific_gallery_gallery($display, $field, $items) {
  $attach = array(
    'attach_skin' => 'default',
  );

  $gallery = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('slick-magnific-gallery'),
      'id' => drupal_html_id('slick-magnific-gallery'),
    ),
    '#attached' => slick_attach($attach),
  );

  $settings = $display['settings'];
  $base_class = _slick_magnific_gallery_base_class($settings, $field);

  $gallery['large-thumbnails'] = _slick_magnific_gallery_render_array($items, $settings, array($base_class));

  if (count($items) > 1) {
    $gallery['small-thumbnails'] = _slick_magnific_gallery_render_array($items, $settings, array(), true);
  }

  return $gallery;
}

function _slick_magnific_gallery_render_array($items, $settings, $class = array(), $small = false) {
  $size = $small ? 'small' : 'large';

  $class = array_merge($class, array('slick-magnific-gallery-' . $size . '-thumbnails'));

  $render_array = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => $class,
    ),
  );

  foreach ($items as $delta => $item) {
    if ($small) {
      $render_array['item-' . $delta] = _slick_magnific_gallery_small_thumbnail($delta, $item, $settings);
    } else {
      $render_array['item-' . $delta] = _slick_magnific_gallery_large_thumbnail($delta, $item, $settings);
    }

  }

  return $render_array;
}

function _slick_magnific_gallery_small_thumbnail($delta, $item, $settings) {
  $render_item = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('small-thumbnail')),
  );

  $render_item['slide'] = array(
    '#theme' => array('figure', 'image'),
    '#path' => $item['small_thumbnail_path'],
    '#alt' => $item['alt'],
    '#title' => $item['title'],
  );

  return $render_item;
}

function _slick_magnific_gallery_large_thumbnail($delta, $item, $settings) {
  $render_item = array(
    '#theme' => 'link',
    '#text' => '',
    '#path' => $item['target_uri'],
    '#options' => array(
      'attributes' => array('class' => array('mfp-item'), 'style' => array()),
      'html' => TRUE,
    ),
  );

  $render_item['#text'] = _slick_magnific_gallery_render_image($item);

  return $render_item;
}

/**
 * @param $item
 * @return string
 */
function _slick_magnific_gallery_render_image($item) {
  $image = array(
    '#theme' => array('figure', 'image'),
    // Try to use a "figure" theme implementation if it exists, or fall back to "image".
    '#path' => $item['large_thumbnail_path'],
    '#alt' => $item['alt'],
    '#title' => $item['title'],
    '#attributes' => array(
      'class' => array('mfp-thumbnail'),
    ),
  );

  return render($image);
}
