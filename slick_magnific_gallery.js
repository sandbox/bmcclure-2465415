/**
 * Created by BMcClure on 4/5/2015.
 */

(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.slickMagnificGallery = {
    attach: function (context, settings) {
      $('.slick-magnific-gallery', context).each(function () {
        var containerId = $(this).attr('id');
        var large = '.slick-magnific-gallery-large-thumbnails';
        var small = '.slick-magnific-gallery-small-thumbnails';

        var largeThumbs = $(large, this);
        var smallThumbs = $(small, this);

        largeThumbs.slick({
          arrows: false
          //asNavFor: '#' + containerId + ' ' + small,
        });

        if (smallThumbs.length > 0) {
          /*smallThumbs.slick({
            slidesToShow: 5
            //asNavFor: '#' + containerId + ' ' + large
          });*/

          $(smallThumbs).children().first().addClass('active');

          largeThumbs.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            /*smallThumbs.slick('slickGoTo', nextSlide, true);*/

            $(smallThumbs).children().removeClass('active');
            $(smallThumbs).children(':eq(' + nextSlide + ')').addClass('active');
          });

          $(smallThumbs).children().on('click', function () {
            var index = $(this).index();

            largeThumbs.slick('slickGoTo', index, true);
          });
        }
      });
    }
  };
})(jQuery, Drupal, this, this.document);
